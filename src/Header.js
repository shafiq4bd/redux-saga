import React from "react";
import { BsFillCartPlusFill } from "react-icons/bs";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const Header = () => {
  const result = useSelector((state) => state.cartReducer);
  console.warn("result", result);

  return (
    <div className="header-area">
      <Link to="/">
        <h1>TechLog Bd</h1>
      </Link>
      <Link to="cart" className="cart-area">
        <BsFillCartPlusFill className="cart-icon" />
        <span> {result.length ? result.length : 0} </span>
      </Link>
    </div>
  );
};

export default Header;
