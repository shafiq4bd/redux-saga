import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./App.css";
import { addToCart, getProductList, removeToCart } from "./redux/action";

const Main = () => {
  const data = useSelector((state) => state.productReducer);
  console.log("data in main component", data);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProductList());
  }, []);

  return (
    <div>
      {/* <button onClick={() => dispatch(emptyToCart())}>Empty to Cart</button> */}

      <div className="product-wrap">
        {data &&
          data.map((item) => (
            <div className="item" key={item.id}>
              <img src={item.image} />
              <div className="top-area">
                <p> {item.category} </p>
                <h2> {item.price} </h2>
              </div>
              <h4> {item.title} </h4>
              <p>{item.description.substring(0, 100)}</p>
              <button onClick={() => dispatch(addToCart(item))}>Add to Cart</button>
              <button onClick={() => dispatch(removeToCart(item.id))}>Remove to Cart</button>
            </div>
          ))}
      </div>
    </div>
  );
};

export default Main;
