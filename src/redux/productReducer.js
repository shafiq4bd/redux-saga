import { SET_PRODUCT_LIST } from "./constants";

export const productReducer = (data = [], action) => {
    console.log("product reducer called", action);
    switch (action.type) {
        case SET_PRODUCT_LIST:
            return [...action.data];
        default:
            return data;
    }

}