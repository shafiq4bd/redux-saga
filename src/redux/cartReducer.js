import { ADD_TO_CART, REMOVE_TO_CART, EMPTY_TO_CART } from "./constants";
export const cartReducer = (data = [], action) => {
    console.log("cart reducer called", action);
    switch (action.type) {
        case ADD_TO_CART:
            console.log("ADD_TO_CART called", action);
            return [action.data, ...data];

        case REMOVE_TO_CART:
            console.log("REMOVE_TO_CART called", action);
            // data.length = data.length ? data.length - 1 : [];
            const remain = data.filter((item)=>item.id !== action.data);
            return [...remain];
        case EMPTY_TO_CART:
            console.log("EMPTY_TO_CART called", action);
            data=[];
            return [...data];
        default:
            return data;
    }

}