import { ADD_TO_CART, REMOVE_TO_CART, EMPTY_TO_CART,   PRODUCT_LIST} from "./constants";

export const addToCart = (data) => {
    console.warn("add action called", data);

    return {
        type: ADD_TO_CART,
        data
    }
}
export const removeToCart = (data) => {
    console.warn("remove action called", data);
    return {
        type: REMOVE_TO_CART,
        data
    }
}

export const emptyToCart = (data) => {
    console.warn("empty action called", data);

    return {
        type: EMPTY_TO_CART,
        data
    }
}

export const getProductList = () => {
    return {
        type: PRODUCT_LIST,
    }
}