import {put, take, takeEvery} from "redux-saga/effects"
import {PRODUCT_LIST, SET_PRODUCT_LIST} from "./constants"

function* getProductList(){
    const data = yield fetch('https://fakestoreapi.com/products').then(res=>res.json());
    console.log("product saga data", data);
    yield put({type: SET_PRODUCT_LIST, data});
}

function* productSaga(){
    yield takeEvery(PRODUCT_LIST, getProductList)
}

export default productSaga;