import React from "react";
import { useSelector } from "react-redux";

const Cart = () => {
  const cartData = useSelector((state) => state.cartReducer);
  const amount = cartData.length && cartData.map((item) => item.price).reduce((prev, next) => prev + next);

  return (
    <div className="cart-details">
      <table>
        <tr>
          <th>#Id</th>
          <th>Title</th>
          <th>Category</th>
          <th>Price</th>
        </tr>
        {cartData &&
          cartData.map((item) => (
            <tr key={Math.random()}>
              <td>{item.id}</td>
              <td>{item.title}</td>
              <td>{item.category}</td>
              <td>{item.price}</td>
            </tr>
          ))}
      </table>
      <div className="discount-area">
        <div className="discount">
          <span>Amount</span> <span>{amount}</span>
        </div>
        <div className="discount">
          <span>Discount</span> <span>{amount / 10}</span>
        </div>
        <div className="discount">
          <span>Tax</span> <span>00</span>
        </div>
        <div className="discount">
          <span>Total</span> <span>{amount - amount / 10}</span>
        </div>
      </div>
    </div>
  );
};

export default Cart;
